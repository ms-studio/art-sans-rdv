<?php
/**
 * @package    WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); ?>

<div id="content" class="content">

	<div id="main" role="main" class="main">
	
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<header>
					<h1 class="h1"><?php the_title(); ?></a></h1>
				</header>
				<?php the_content( 'Read the rest of this entry &raquo;' ); ?>
				
	
			</article>
	
		<?php endwhile;
		else: ?>
	
			<p>Sorry, no posts matched your criteria.</p>
	
		<?php endif; ?>
	
	</div>

</div><!-- end of #content -->

<?php get_footer(); ?>
