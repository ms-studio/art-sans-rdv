jQuery(document).ready(function($){

/*
 * CONTENTS
 *
 *   1: Email Spam Protection 
 *
 */


/* 
 * 0: js-hidden must be hidden
 ****************************************************
 */
 // $(".js-hidden").hide();


/*
 * 4.
 * Randomize Header Logo
*/

// 1: LogoNr = generate a random value
// 2: add it to body

var randIcon = Math.floor((Math.random() * 5) + 1);

if ( $( "body" ).is( ".home" ) ) {
 // do nothing
} else {
	$( "body" ).addClass( "logo-" + randIcon );
}




/* 
 * Outgoing Links = new window
 ****************************************************
 */

$("a[href^=http]").each(
	function(){
		if(this.href.indexOf(location.hostname) === -1) {
			$(this).attr('target', '_blank');
		}
	}
 );


/* 
 * 1.
 * EmailSpamProtection (jQuery Plugin)
 ****************************************************
 * Author: Mike Unckel
 * Source: http://unckel.de/labs/jquery-plugin-email-spam-protection
 *
 * How to use: Write each email in HTML in this way: 
 * <span class="email">info [at] domain.com</span>
 */
$.fn.emailSpamProtection = function(className) {
	return $(this).find("." + className).each(function() {
		var $this = $(this);
		var s = $this.text().replace(" [at] ", "&#64;");
		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
	});
};
$("body").emailSpamProtection("email");

/* 
 * 2.
 * Mobile Menu
 ****************************************************
 * Show/hide the menu when clicking on #mobile-links a.link-menu
 */

$("#mobile-links").on("click", "a.link-menu-closed", function(){
		 $(this).removeClass("link-menu-closed");
		 $(this).addClass("link-menu-open");
		 $("#mobile-menu").show();
		 $(this).next(".sub-article").show('200', function() {
		 // Animation complete.	
		 });
		 return false;
 });

$("#mobile-links").on("click", "a.link-menu-open", function(){
		 $(this).removeClass("link-menu-open");
		 $(this).addClass("link-menu-closed");
		 $("#mobile-menu").hide();
		 $(this).next(".sub-article").hide('200', function() {
		 // Animation complete.
		 });
		 return false;
 });

/* 
 * 3.
 * Download links
 ****************************************************
 * Show the download links when clicking on #download .call
 */

$("#download").on("click", ".call", function(){
		 $("#download").removeClass("inactive");
		 $("#download").addClass("active");
		 $("#app-menu").show();
		 return false;
 });



// IDEA: tester aussi la hauteur, définir une hauteur minimum, pour le device ET le contenu

var intViewportHeight = $(window).height(); // window.innerHeight;
var contentHeight = $('#content').height();

if ( ( document.documentElement.clientWidth > 767 ) && ( intViewportHeight > 600 ) && ( contentHeight > 700 ) ) {

		// $(document.body).append("<div id='testing' class='testing'><div id='testing-inside'></div></div>");
		
		var header = $('#header');
		var pos = header.offset();
		var headerpos = pos.top+header.height();
		
		var headerpos = 80;
		var headeroffset = header.height() + 30;
		
		$(window).scroll( function() {
		      
		      	var testthis = $(window).scrollTop();
		      	
		      	// $("#testing-inside").html(testthis+" / "+headerpos);
		      	
		      	if ( testthis > headerpos ) {
		      	
		      	    //header.removeClass('default').addClass('fixed-menu');
		      			$("#container").addClass('fixed-header');
		      			
		      			// add margin to content!
		      			$("#content").css("margin-top",headeroffset+"px");
		      			
		      	} else {
		      			
		      			$("#container").removeClass('fixed-header');
		      	    // header.removeClass('fixed-menu').addClass('default');
		      			$("#content").css("margin-top", "0px");
		      	}
		      	
		} ); // end window scroll

}

/* 
 * that's it !
 ****************************************************
 */

}); // end document ready
