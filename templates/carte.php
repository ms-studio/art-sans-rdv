<?php
/**
Template Name: Carte
*
* @package artsansrdv
*
*/

get_header(); ?>

<div id="content" class="content">

<div id="main" role="main" class="main">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
		
		<div id="artmap" class="artmap">
		
		<?php 
		
		// Query for OEUVRES.
		
		$custom_query = new WP_Query( array(
  					'post_type' => 'artwork',
  					'posts_per_page' => -1,
  					'orderby'  => 'name',
  					'order'  => 'DESC', //
  			) ); 
  			
  			if ($custom_query->have_posts()) : 
  		?>
  		<div id="mapitems">
  		<?php 
  		while( $custom_query->have_posts() ) : $custom_query->the_post();
  					
  					// Get ACF fields
  
  					echo '<div class="spot id-'.get_the_ID().'" data-slug="'.$post->post_name.'"';
  					
  					if ( get_field('asr_auteur') )
  					{
  						echo ' data-artist="' . get_field('asr_auteur') . '"';
  					}
  					
  					if ( get_field('asr_year') )
  					{
  						echo ' data-year="' . get_field('asr_year') . '"';
  					}
  					
  					if ( get_field('asr_descr') )
  					{
  						echo ' data-descr="' . get_field('asr_descr') . '"';
  					}
  					
  					// obtenir les pictos:
  					
  					$image = get_field('image');
  					
  					if( !empty($image) ): ?>
  					
  						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
  					
  					<?php endif;
  					
  					$asr_picto_blanc = get_field('asr_picto_blanc');
  					
  					// reset values
  					$picto_format = '';
  					
  					if ( !empty($asr_picto_blanc) )
  					{
  						echo ' data-picto-blanc="' . $asr_picto_blanc['url'] . '"';
  						
  						$picto_width = $asr_picto_blanc['sizes']['large-width'];
  						$picto_height = $asr_picto_blanc['sizes']['large-height'];
  						
  						$picto_width = round($picto_width/2);
  						$picto_height = round($picto_height/2);
  						
  						if ( $picto_width >= $picto_height ) {
  							$picto_format = 'horizontal';
  						} else {
  							$picto_format = 'vertical';
  						}
  						
  						echo ' data-picto-w="' . $picto_width . '"';
  						echo ' data-picto-h="' . $picto_height . '"';
  						echo ' data-picto-format="' . $picto_format . '"';
  					}
  					
  					$asr_picto_noir = get_field('asr_picto_noir');
  					
  					if ( !empty($asr_picto_noir) )
  					{
  						echo ' data-picto-noir="' . $asr_picto_noir['url'] . '"';
  					}
  					
  					echo '>'.get_the_title().'</div>
  					';
  					
    			
     	endwhile; ?>
  		 </div>
  		 <?php
  		 
  		endif;
  		wp_reset_postdata();
		
		 ?>
			
			<img class="artmap-img" src="<?php echo get_stylesheet_directory_uri().'/img/map/map-plaine-transp.png' ?>" alt="Plaine de Plainpalais" />
		</div>
		
		
		<script>
		jQuery(document).ready(function($){

			// create thickbox
			// reference: https://github.com/ms-studio/scribus-planet/blob/master/js/scripts.js
			
			$(document.body).append("<div id='modalbox' class='modalbox hidden'><div class='modalbox-inside'><div id='modalbox-insidest' class='modalbox-insidest clear'></div><div id='close-button' class='close-button'></div></div></div>");
			// 
			$(document.body).append("<div id='mask' class='mask-layer hidden'></div>");
			
			(function( $ ){
			  $.fn.closeReader = function() {
			  		  $("#modalbox").fadeOut(300);
			  		  $("#mask").fadeOut(300);
			  		  return false;
			  };
			})( jQuery );
			
			var asr_slctr = $("#mapitems div.spot").first();
			
			var asr_orig_bg = asr_slctr.css( "background-image" );
			var asr_orig_w = asr_slctr.css( "width" );
			var asr_orig_h = asr_slctr.css( "height" );
			var asr_orig_top = asr_slctr.css( "margin-top" );
			var asr_orig_left = asr_slctr.css( "margin-left" );
			var asr_orig_bg_sz = asr_slctr.css( "background-size" );
			// background-size
			// margin-top
			// margin-left
			
			$("#mapitems").on("mouseenter", "div.spot", function() {
				
					var asr_title = $(this).text();
					var asr_artist = $(this).data("artist");
					var asr_year = $(this).data("year");
					var asr_descr = $(this).data("descr");
					var asr_slug = $(this).data("slug");
					var asr_picto_url = $(this).data("picto-noir");
					var asr_picto_w = ($(this).data("picto-w"))/3;
					var asr_picto_h = ($(this).data("picto-h"))/3;
					var asr_picto_left = (-asr_picto_w)/2;
					var asr_picto_top = (-asr_picto_h)/2;
					var asr_picto_format = $(this).data("picto-format");
					
					var asr_picto_url = 'url('+asr_picto_url+')';
					var asr_bg_sz = asr_picto_w+'px '+asr_picto_h+'px';					
					
					// alert(asr_picto_url);
					
					$(this).css({
//					       'background-color': 'rgba(255,255,255,0.6)',
					      'background-image': asr_picto_url,
					      'width' : asr_picto_w,
					      'height' : asr_picto_h,
					      'background-size' : asr_bg_sz,
					      'margin-top' : asr_picto_top,
					      'margin-left' : asr_picto_left
					    });
			
			}).on("mouseleave", "div.spot", function(){
				    // restore values
				    
				     // alert("width:"+asr_orig_w);
				    
				    $(this).css({
				           'background-color': 'transparent',
				          'background-image': asr_orig_bg,
				          'background-size': asr_orig_bg_sz,
				          'width' : asr_orig_w,
				          'height' : asr_orig_h,
				          'margin-top' : asr_orig_top,
				          'margin-left' : asr_orig_left
				    });
				    
			}); // end ON HOVER
			
			$("#mapitems").on("click", "div.spot", function() {
			
							var asr_title = $(this).text();
							var asr_artist = $(this).data("artist");
							var asr_year = $(this).data("year");
							var asr_descr = $(this).data("descr");
							var asr_slug = $(this).data("slug");
							var asr_picto_url = $(this).data("picto-blanc");
							var asr_picto_w = $(this).data("picto-w");
							var asr_picto_h = $(this).data("picto-h");
							var asr_picto_format = $(this).data("picto-format");
							
							$('#modalbox-insidest').empty().removeClass("horizontal vertical");
							$('#modalbox-insidest').addClass( asr_picto_format ).append('<h1 class="title">'+asr_title+'</h1><div class="artwork-img-box '+asr_picto_format+'" style="max-width:'+asr_picto_w+'px;max-height='+asr_picto_h+'px"><img class="artwork-img" src="'+asr_picto_url+'?v=5" /></div><div class="artwork-info"><div class="artist">'+asr_artist+'</div><div class="year">'+asr_year+'</div><div class="description">'+asr_descr+'</div></div>');
							// $('#modalbox-insidest').append("<div id='close-button' class='close-button'></div>");
									    
							$('#modalbox').fadeIn(300);
							$('#mask').fadeIn(300);
							
							// $('#modalbox .reader-inside').html(clonage);
							
							// $('#reader .post-content').css({'height': 'auto','max-height': new_container_height+'px'});
							
							// alert("item is: " + window.item_nr);
							return false;
			}); // end ON CLICK
		
			/*  
			 What happens when we close the reader
			*/
			
			$("#close-button").on("click", function() {
				$(document).closeReader();
			});
			
			// Use the ESC key:
			$(document).keyup(function(e) {
			  if (e.keyCode == 27) { 
			  	$(document).closeReader();
			  }
			});
			
		
		});
		
		</script>
		
	

</div><!-- #main -->

</div><!-- #content -->

<?php get_footer(); ?>
