<?php
/**
Template Name: StoryMap
*
* This template is used to display a StoryMap.
*
* @package artsansrdv
*
*/

// enqueue scripts 

get_header(); ?>

<div id="content" class="content">

<div id="main" role="main" class="main">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="h1"><?php the_title(); ?></a></h1>
			</header>
			
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
			
			
			<div id="mapdiv" style="width: 100%; height: 600px;"></div> 
			
			<!-- Your script tags should be placed before the closing body tag. -->
			
			<script type="text/javascript" src="http://cdn.knightlab.com/libs/storymapjs/latest/js/storymap-min.js"></script>
			
			<script>
			// storymap_data can be an URL or a Javascript object
			// var storymap_data = '<?php echo get_stylesheet_directory_uri(); ?>/js/storymap-published-readable.json'; 
			var storymap_data = '//s3.amazonaws.com/uploads.knightlab.com/storymapjs/1788a935d4d877897b5688ab20905c5e/art-sans-rdv/published.json';
			
			// certain settings must be passed within a separate options object
			var storymap_options = {};
			
			var storymap = new VCO.StoryMap('mapdiv', storymap_data, storymap_options);
			window.onresize = function(event) {
			    storymap.updateDisplay(); // this isn't automatic
			}          
			</script>
			

		</article>

	<?php endwhile;
	else: ?>

		<p>Sorry, no posts matched your criteria.</p>

	<?php endif; ?>

</div>

</div>

<?php get_footer(); ?>
