<?php
/**
Template Name: Accueil
*
*
*
* @package artsansrdv
*
*/

get_header(); ?>

<div id="content" class="content">

<div id="main" role="main" class="main">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>		
		
		<div class="animation" id="animation">
			<?php 
			
					if (get_field('asr_video_still')) {
					
						$image = get_field('asr_video_still');
						
						if ( !empty($image) ) { ?>
						
						<div id="media-container" class="media-container videoframe">
						
							<div class="video-play-icon"></div>
							<img class="video-preview" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						
						</div>
						
						<?php 
						
							}  // !empty($image)
						} // get_field('asr_video_still')
						
						if (get_field('asr_video_frontpage'))
							{
								$iframe = get_field( 'asr_video_frontpage' );
								
//								preg_match('/src="(.+?)"/', $video, $matches_url );
//								$src = $matches_url[1];	
//								
//								preg_match('/embed(.*?)?feature/', $src, $matches_id );
//								$id = $matches_id[1];
//								$id = str_replace( str_split( '?/' ), '', $id );
								
								// var_dump( $id );
						
						 ?>
						
						<script type="text/javascript">
						jQuery(document).ready(function($){
						
							$('div.videoframe').on("click", function(){ // the trigger action 
							  
							  if ( $(this).hasClass( 'videoframe' ) ) {
							  
											  //  alert("click!");
											  
											  $(this).removeClass("videoframe");
											  $(this).addClass("embed-container");
											  
											  var videokey = $(this).data('video');
											  var videoposter = $(this).data('poster');
											  
											  // get the padding-bottom value
											  // var vimeoratio = $(this).css( "padding-bottom" );
											  
											  // apply it to the parent element
											  // $(this).parent("div.vimeo-inside").css( "padding-bottom", vimeoratio );
											  
											  // was = replaceWith
											  
											  //$video = str_replace('feature=oembed', 'feature=oembed&amp;autoplay=1', $video);
											  
											  // use preg_match to find iframe src
											  <?php
											  
											  preg_match('/src="(.+?)"/', $iframe, $matches);
											  $src = $matches[1];
											  
											  
											  // add extra params to iframe src
											  $params = array(
											      'controls'    => 0,
											      'hd'        => 1,
											      'autohide'    => 1,
											      'autoplay' => 1
											  );
											  
											  $new_src = add_query_arg($params, $src);
											  
											  $iframe = str_replace($src, $new_src, $iframe);
											  
											  ?>
											  // add extra attributes to iframe html
//											  $attributes = 'frameborder="0"';
//											  
//											  $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
											  
											  
											  
											  $(this).html('<?php echo $iframe; ?>');
											  
							  
							  }
							  
							  // $('.close-button').hide();
								//  $('.wp-video-shortcode').mediaelementplayer( settings );
							  return false;
							});
							
						});
						</script>
					<?php
					
				} // end if get_field
					
						
					
			
					if (get_field('asr_video_frontpage'))
						{
							$video = get_field( 'asr_video_frontpage' );
							
							preg_match('/src="(.+?)"/', $video, $matches_url );
							$src = $matches_url[1];	
							
							preg_match('/embed(.*?)?feature/', $src, $matches_id );
							$id = $matches_id[1];
							$id = str_replace( str_split( '?/' ), '', $id );
							
							// var_dump( $id );
							
					} // end if get_field
			


//			$videourl = 'https://www.youtube.com/watch?v=6YoDx54kwjA';
//			
//			$htmlcode = wp_oembed_get($videourl);
//			
//			echo '<div class="media-container youtube-container">';
//			echo $htmlcode;
//			echo '</div>';
			
			 ?>
		</div>

		<article <?php post_class('about') ?> id="post-<?php the_ID(); ?>">
			<?php the_content( 'Read the rest of this entry &raquo;' ); ?>
		</article>
		
	<?php endwhile; ?>

	<?php endif; ?>

</div><!-- #main -->

</div><!-- #content -->

<?php get_footer(); ?>
