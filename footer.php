<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */
?>


<?php 

if ( is_404() ) { 

	// do nothing

} else {

 ?>

<div id="sidebar" class="sidebar">

	<div id="download" class="download-zone inactive">
		<div class="download-container">
				<div class="call call-icon"></div>
				
				<div id="app-menu" class="js-hidden">
				<?php wp_nav_menu( array(
						'theme_location'  => 'download',
						'container'       => 'nav',
						'container_class' => 'app-menu',
						'depth'           => 0,
					//'link_after'       => '&nbsp;',
				) ); ?>
				</div>
				
				<p class="call call-text">Téléchargez l’application</p>
		</div>
	</div>
	
	
	<div id="newsletter" class="newsletter">
	<h3>Newsletter</h3>
	
	<?php 
	
	$widgetNL = new WYSIJA_NL_Widget(true);
	echo $widgetNL->widget(array('form' => 1, 'form_type' => 'php'));
	
	echo '</div>';
	
} 

?>

</div><!-- #sidebar -->

		</div> <!-- .sub-container -->
</div><!-- #container -->

<?php wp_footer(); ?>

</body>
</html>
