<?php
/**
 * @package    WordPress
 * @subpackage HTML5_Boilerplate
 */


require_once( 'functions/init.php' );



/* admin interface
******************************/

if ( is_user_logged_in() ) {
		require_once('functions/admin.php');
}



// end of functions.php