<?php
/**
 * @package    WordPress
 * @subpackage Tabula_Rasa
 */
?>
	<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">

		<title><?php if ( is_front_page() ) {
				bloginfo( 'name' );
				echo " &mdash; ";
				bloginfo( 'description' );
			} else {
				wp_title( '&mdash;', true, 'right' );
				bloginfo( 'name' );
			}
			?></title>

		<?php // ** DESCRIPTION v.0.3 **
		if ( is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post();
			?>
			<meta name="description" content="<?php
			$descr = get_the_excerpt();
			$text = str_replace( '/\r\n/', ', ', trim( $descr ) );
			echo esc_attr( $text );
			?>" />
		<?php endwhile; endif;
		elseif ( is_home() ) :
			?>
			<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
		<?php endif; ?>

		<meta name="author" content="">

		<?php // ** SEO OPTIMIZATION v.0.2 **
		if ( is_attachment() ) {
			?>
			<meta name="robots" content="noindex,follow" /><?php
		} else {
			if ( is_single() || is_page() || is_home() ) {
				?>
				<meta name="robots" content="all,index,follow" /><?php
			} else {
				if ( is_category() || is_archive() ) {
					?>
					<meta name="robots" content="noindex,follow" /><?php
				}
			}
		}
		?>

		<!-- Mobile viewport optimized: h5bp.com/viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="apple-touch-icon" href="/apple-touch-icon.png">

		<style>.hidden {
				display: none;
			}</style>
		<!-- we want this to be hidden immediately before the rest of CSS loads -->


		<!-- Wordpress Head Items -->
		<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

		<?php wp_head(); ?>

	</head>
<body <?php

// init variable
$nfo_body_var = '';

if ( is_single() ) {

	// test categories
	include( get_template_directory() . '/inc/categories-list.php' );

}

// $nfo_body_var .= 'no-js';

body_class( $nfo_body_var );

?>>

<div id="container" class="container">

<?php

if ( is_404() ) { 

	?>
	<header role="banner" class="header">
		<div class="header-container sub-container">
			<h1 class="logo-error">Page non trouvée</h1>
		</div><!-- .header-container -->
	</header>
	<?php

} else {

?>

<div id="mobile-menu" class="mobile-menu js-hidden">
<?php wp_nav_menu( array(
		'theme_location'  => 'mobile',
		'container'       => 'nav',
		'container_class' => 'clear main-menu default-menu horiz-list small-font',
		'depth'           => 0,
	//'link_after'       => '&nbsp;',
) ); ?>
</div>

<header id="header" role="banner" class="header default">
	<div id="header-container" class="header-container sub-container">
	
	<div class="header-content">
	
	<h1 class="logo"><a href="<?php echo get_option( 'home' ); ?>/"><?php bloginfo( 'name' ); ?></a></h1>
	
	
		<div id="mobile-links" class="mobile-links">
			<a href="#" class="link-menu link-menu-closed">Menu</a>
			<a href="#"class="link-capsules">Capsules</a>
		</div>
		
		<div id="desktop-menu" class="desktop-menu">
		<?php wp_nav_menu( array(
				'theme_location'  => 'desktop',
				'container'       => 'nav',
				'container_class' => 'clear main-menu default-menu horiz-list small-font',
				'depth'           => 0,
			//'link_after'       => '&nbsp;',
		) ); ?>
		</div>
		
		<div id="social-menu" class="social-menu">
		<?php wp_nav_menu( array(
				'theme_location'  => 'social',
				'container'       => 'nav',
				'container_class' => 'clear main-menu default-menu horiz-list small-font',
				'depth'           => 0,
			//'link_after'       => '&nbsp;',
		) ); ?>
		</div>
		
		<div id="search-form-desktop" class="search-form-desktop">
			<?php get_search_form(); ?>
		</div>
	</div><!-- .header-content -->
</div><!-- .header-container -->
</header>

<?php 

} // testing for 404

 ?>
<div class="sub-container">
