<?php
/**
 * @package WordPress
 * @subpackage HTML5_Boilerplate
 */

get_header(); ?>

<div id="content" class="content">

  <div id="main" role="main" class="main">

  <?php if (have_posts()) : ?>

    <h2 class="h2">Résultats de la recherche</h2>

    <nav>
      <div><?php next_posts_link('&laquo; Older Entries') ?></div>
      <div><?php previous_posts_link('Newer Entries &raquo;') ?></div>
    </nav>

    <?php while (have_posts()) : the_post(); ?>

      <article <?php post_class() ?>>
        <h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
        <time>Publié le <?php the_time('l j F  Y') ?></time>

      </article>

    <?php endwhile; ?>

    <nav>
      <div><?php next_posts_link('&laquo; Older Entries') ?></div>
      <div><?php previous_posts_link('Newer Entries &raquo;') ?></div>
    </nav>

  <?php else : ?>

    <h2 class="h2">Pas de résultat trouvé</h2>
    <?php get_search_form(); ?>

  <?php endif; ?>

  </div>

</div>

<?php get_footer(); ?>
